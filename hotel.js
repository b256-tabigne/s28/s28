// Hotel Database

// Single Room
db.user.insertOne({
	"name": "single",
	"accommodates": 2,
	"price": 1000,
	"description": "A simple room with all the basic necessities",
	"rooms_available": 10,
	"isAvailable": false
	})

// Double Room & Queen Room
db.user.insertMany([
        {
	"name": "double",
	"accommodates": 3,
	"price": 2000,
	"description": "A room fit for a small family going on a vacation",
	"rooms_available": 5,
	"isAvailable": false
	},
	{
	"name": "queen",
	"accommodates": 3,
	"price": 4000,
	"description": "A room with a queen sized bed perfect for a simple getaway",
	"rooms_available": 15,
	"isAvailable": false
	}
        ])


// Find

db.user.insertOne({"name": "double"})

// Update

db.user.updateOne(
    {
        "rooms_available": 15
        }
    {
    $set: {
        "rooms_available": 0
        }
    }

)

// Delete

db.user.deleteOne(
    {
        "rooms_available": 0
        }
)